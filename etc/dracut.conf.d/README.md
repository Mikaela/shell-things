My dracut configuratino files mainly for generating unified kerneil images
(uki).

# WARNING!

Sedric has a 96M EFI partition courtesy of Windows and thus it has a lot of
attempts for decreasing the kernel size. Since moving it to UKI, I am yet to
go through what of it is actually useful and worth keeping around and at least
disabling recovery seems dangerous if I can save space by omitting somnething
else.
